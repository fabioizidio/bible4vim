function! bible4vim#Bible(...) abort
  let book = a:1
  let chapter = a:2
  let verse = a:3
  let command = "/home/fabioizidio/biblia.sh " . book . " " . chapter . " " . verse
  let result = system(command)

  let text = "\"" . result . "\" -- (" . book . " " . chapter . ":" . verse . ")" 

  echo text
endfunction

command! -nargs=* Bible call bible4vim#Bible(<f-args>)

